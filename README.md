# TSP

## Requirements:

It uses PHP 8.2.
If you have any issues with it, I can provide a docker-compose configuration I used.

## Script

Script for running inside `public`.
Put the file with transactions to the same dir.

Run script:

```shell
php public/app.php input.txt
```

## Configuration:

I didn't make some Factory, or DI-Config for create an application.
Inside the `public/app.php` you can configure or switch some implementation for some 
services.

## Test run by:

```shell
vendor/bin/phpunit
```
