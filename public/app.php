<?php

use App\Application\Core;
use App\Domain\Enums\Currency;
use App\Domain\Services\Commissions\CommissionCalculator;
use App\Domain\Services\Commissions\CommissionConfiguration;
use App\Domain\Services\Commissions\CommissionRuleByCountry;
use App\Domain\Services\Currencies\CurrencyConverter;
use App\Infrastructure\CountryResolvers;
use App\Infrastructure\CurrencyConverters;
use App\Infrastructure\Repositories\Transactions\TransactionFileRepository;
use Symfony\Component\HttpClient\HttpClient;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$file = __DIR__ . '/' . $argv[1];

//// for e.g.: api.apilayer.com
//$clientParams = [
//    'headers' => [
//        'Content-Type' => 'text/plain',
//        'apikey: your-api-key',
//    ],
//    'base_uri' => 'https://api.apilayer.com/exchangerates_data/'
//];
//$currencyApiManager = new CurrencyConverters\ApiLayerManager(HttpClient::create($clientParams));

// for e.g.: open.er-api.com
$clientParams = ['base_uri' => 'https://open.er-api.com/v6/'];
$currencyApiManager = new CurrencyConverters\ErManager(HttpClient::create($clientParams));


$countryApiManager = new CountryResolvers\BinList(HttpClient::create(['base_uri' => 'https://lookup.binlist.net/']));

$calculator = new CommissionCalculator(
    new CommissionConfiguration($countryApiManager, new CommissionRuleByCountry()),
    new CurrencyConverter($currencyApiManager),
    Currency::EUR,
);

$repository = new TransactionFileRepository($file);
(new Core($repository, $calculator))->handle();
