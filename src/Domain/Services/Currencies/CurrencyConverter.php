<?php

declare(strict_types=1);

namespace App\Domain\Services\Currencies;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

readonly class CurrencyConverter
{
    public function __construct(
        private CurrencyConverterManagerInterface $client,
    ) {
    }

    /**
     * @param Transaction $transaction
     * @param Currency $base
     *
     * @return Transaction
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function convert(Transaction $transaction, Currency $base): Transaction
    {
        if ($transaction->getCurrency() === $base) {
            return $transaction;
        }

        $amount = $this->client->convert($transaction->getAmount(), $transaction->getCurrency(), $base);

        return new Transaction($transaction->getBin(), $amount, $base);
    }
}
