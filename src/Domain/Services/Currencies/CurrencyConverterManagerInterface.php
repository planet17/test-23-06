<?php

declare(strict_types=1);

namespace App\Domain\Services\Currencies;

use App\Domain\Enums\Currency;

interface CurrencyConverterManagerInterface
{
    /**
     * @param float $amount
     * @param Currency $from
     * @param Currency $to
     *
     * @return float
     */
    public function convert(float $amount, Currency $from, Currency $to): float;
}
