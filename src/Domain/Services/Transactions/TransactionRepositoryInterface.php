<?php

declare(strict_types=1);

namespace App\Domain\Services\Transactions;

use App\Domain\Dto\Transaction;

interface TransactionRepositoryInterface
{
    /**
     * @return Transaction[]
     */
    public function all(): array;

}
