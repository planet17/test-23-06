<?php

declare(strict_types=1);

namespace App\Domain\Services\Commissions;

use App\Domain\Dto\Country;
use App\Domain\Dto\Transaction;

interface CountryResolverInterface
{
    public function resolve(Transaction $transaction): Country;
}
