<?php

declare(strict_types=1);

namespace App\Domain\Services\Commissions;

use App\Domain\Dto\Transaction;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

readonly class CommissionConfiguration
{
    public function __construct(
        private CountryResolverInterface $countryResolver,
        private CommissionRuleByCountry $serviceRuleByCountry,
    ) {
    }

    /**
     * @param Transaction $transaction
     *
     * @return float
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function get(Transaction $transaction): float
    {
        $country = $this->countryResolver->resolve($transaction);

        return $this->serviceRuleByCountry->match($country);
    }
}
