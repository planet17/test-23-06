<?php

declare(strict_types=1);

namespace App\Domain\Services\Commissions;

use App\Domain\Dto\Country;

class CommissionRuleByCountry
{
    private const EU_COUNTRY_LIST = [
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'ES',
        'FI',
        'FR',
        'GR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PO',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK',
    ];

    public function match(Country $item): float
    {
        return match (true) {
            in_array($item->getCode(), self::EU_COUNTRY_LIST) => 0.01,
            default => 0.02,
        };
    }
}
