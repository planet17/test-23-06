<?php

declare(strict_types=1);

namespace App\Domain\Services\Commissions;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Domain\Services\Currencies\CurrencyConverter;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

readonly class CommissionCalculator
{
    public function __construct(
        private CommissionConfiguration $commissionConfiguration,
        private CurrencyConverter $currencyConverter,
        private Currency $baseCommissionCurrency,
    ) {
    }

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function calculate(Transaction $transaction): float
    {
        $converted = $this->currencyConverter->convert($transaction, $this->baseCommissionCurrency);
        $commission = $this->commissionConfiguration->get($converted);

        return round($converted->getAmount() * $commission, 2, PHP_ROUND_HALF_UP);
    }
}
