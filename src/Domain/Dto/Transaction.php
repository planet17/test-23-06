<?php

declare(strict_types=1);

namespace App\Domain\Dto;

use App\Domain\Enums\Currency;

readonly class Transaction
{
    public function __construct(
        private string $bin,
        private float $amount,
        private Currency $currency,
    ) {
    }

    /**
     * @return string
     */
    public function getBin(): string
    {
        return $this->bin;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}
