<?php

declare(strict_types=1);

namespace App\Domain\Dto;

readonly class Country
{
    public function __construct(private string $code)
    {
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
