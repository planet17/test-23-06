<?php

declare(strict_types=1);

namespace App\Infrastructure\CountryResolvers;

use App\Domain\Dto\Country;
use App\Domain\Dto\Transaction;
use App\Domain\Services\Commissions\CountryResolverInterface;
use JsonException;
use RuntimeException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly class BinList implements CountryResolverInterface
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @param Transaction $transaction
     *
     * @return Country
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resolve(Transaction $transaction): Country
    {
        $response = $this->client->request('GET', $transaction->getBin());
        $responseData = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $code = $responseData['country']['alpha2'] ?? null;
        if (!$code) {
            throw new RuntimeException('Code not found at response');
        }

        return new Country($code);
    }
}
