<?php

declare(strict_types=1);

namespace App\Infrastructure\Repositories\Transactions;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Domain\Exception\InvalidTransactionRowException;
use App\Domain\Services\Transactions\TransactionRepositoryInterface;
use InvalidArgumentException;
use JsonException;

readonly class TransactionFileRepository implements TransactionRepositoryInterface
{
    public function __construct(private string $filePath)
    {
        $this->preValidate();
    }

    /**
     * @return Transaction[]
     * @throws JsonException
     */
    public function all(): array
    {
        $items = [];
        foreach (explode("\n", file_get_contents($this->filePath)) as $row) {
            if (empty($row)) {
                break;
            }

            $data = json_decode($row, true, 512, JSON_THROW_ON_ERROR);
            $this->validateRowData($data);
            $items[] = $this->transform($data);
        }

        return $items;
    }

    private function validateRowData(array $data): void
    {
        if (!array_key_exists('amount', $data)) {
            throw new InvalidTransactionRowException('Not found amount');
        }

        if (!array_key_exists('bin', $data)) {
            throw new InvalidTransactionRowException('Not found bin');
        }

        if (!array_key_exists('currency', $data)) {
            throw new InvalidTransactionRowException('Not found currency');
        }

        if (!Currency::tryFrom($data['currency'])) {
            throw new InvalidTransactionRowException('Invalid currency code: ' . $data['currency']);
        }

        if (!is_numeric($data['amount'])) {
            throw new InvalidTransactionRowException('Invalid amount: ' . $data['amount']);
        }

        if (!is_numeric($data['bin'])) {
            throw new InvalidTransactionRowException('Invalid bin: ' . $data['bin']);
        }
    }

    private function transform(array $data): Transaction
    {
        $currency = Currency::from($data['currency']);

        return new Transaction($data['bin'], (float)$data['amount'], $currency);
    }

    private function preValidate(): void
    {
        if (!file_exists($this->filePath)) {
            throw new InvalidArgumentException('File not exists: ' . $this->filePath);
        }
    }
}
