<?php

declare(strict_types=1);

namespace App\Infrastructure\CurrencyConverters;

use App\Domain\Enums\Currency;
use App\Domain\Exception\InvalidApiResponseException;
use App\Domain\Services\Currencies\CurrencyConverterManagerInterface;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly class ApiLayerManager implements CurrencyConverterManagerInterface
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @inheritDoc
     *
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function convert(float $amount, Currency $from, Currency $to): float
    {
        $response = $this->client->request('GET', 'latest', [
            'query' => ['base' => $to->value, 'symbols' => $this->prepareAllSymbols()],
        ]);

        $content = $response->getContent();
        $responseData = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $this->validateData($responseData, $from->value);

        $rate = $responseData['rates'][$from->value] ?? 0.0;

        if ($rate > 0) {
            return $amount / $rate;
        }

        return $amount;
    }

    private function prepareAllSymbols(): string
    {
        $symbols = Currency::cases();
        $symbols = array_map(static function ($item) { return $item->value; }, $symbols);

        return implode(',', $symbols);
    }

    /**
     * @throws JsonException
     */
    private function validateData(mixed $responseData, string $currencyCode): void
    {
        if (!array_key_exists('rates', $responseData)) {
            throw new InvalidApiResponseException(json_encode($responseData, JSON_THROW_ON_ERROR));
        }

        /* Must work like example - don't check it */
        /* if (!array_key_exists($currencyCode, $responseData['rates'])) {
            throw new InvalidApiResponseException($currencyCode . ' not found at response');
        }*/
    }
}
