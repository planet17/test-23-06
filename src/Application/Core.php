<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Services\Commissions\CommissionCalculator;
use App\Domain\Services\Transactions\TransactionRepositoryInterface;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

readonly class Core
{
    public function __construct(
        private TransactionRepositoryInterface $transactionRepository,
        private CommissionCalculator $calculator,
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function handle(): void
    {
        foreach ($this->transactionRepository->all() as $transaction) {
            $amount = $this->calculator->calculate($transaction);
            echo "{$amount}\n";
        }
    }
}
