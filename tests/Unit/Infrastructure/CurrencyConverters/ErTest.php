<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\CurrencyConverters;

use App\Domain\Enums\Currency;
use App\Domain\Exception\InvalidApiResponseException;
use App\Infrastructure\CurrencyConverters\ErManager;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ErTest extends TestCase
{
    private const RESPONSE_CONTENT_BODY = '{ "result": "success", "provider": "https://www.exchangerate-api.com", "documentation": "https://www.exchangerate-api.com/docs/free", "terms_of_use": "https://www.exchangerate-api.com/terms", "time_last_update_unix": 1690156951, "time_last_update_utc": "Mon, 24 Jul 2023 00:02:31 +0000", "time_next_update_unix": 1690244861, "time_next_update_utc": "Tue, 25 Jul 2023 00:27:41 +0000", "time_eol_unix": 0, "base_code": "EUR", "rates": { "EUR": 1, "USD": 1.112605 } }';

    /**
     * @covers ErManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testCurrencyConversionWithValidExchangeRate(): void
    {
        $amount = 100;
        $from = Currency::USD;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn(self::RESPONSE_CONTENT_BODY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ErManager($httpClient);
        $convertedAmount = $apiLayerManager->convert($amount, $from, $to);

        $this->assertEqualsWithDelta(89.87, $convertedAmount, 0.01);
    }

    /**
     * @covers ErManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testCurrencyConversionWithSameCurrencyValidExchangeRate(): void
    {
        $amount = 100;
        $from = Currency::EUR;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn(self::RESPONSE_CONTENT_BODY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ErManager($httpClient);
        $convertedAmount = $apiLayerManager->convert($amount, $from, $to);

        $this->assertEqualsWithDelta(100, $convertedAmount, 0.01);
    }

    /**
     * @covers ErManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testException(): void
    {
        $this->expectException(InvalidApiResponseException::class);
        $amount = 100;
        $from = Currency::EUR;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn('{}');

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ErManager($httpClient);
        $apiLayerManager->convert($amount, $from, $to);
    }
}
