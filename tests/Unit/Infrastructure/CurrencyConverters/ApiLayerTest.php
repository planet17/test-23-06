<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\CurrencyConverters;

use App\Domain\Enums\Currency;
use App\Domain\Exception\InvalidApiResponseException;
use App\Infrastructure\CurrencyConverters\ApiLayerManager;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiLayerTest extends TestCase
{
    private const RESPONSE_CONTENT_BODY = '{ "success": true, "timestamp": 1690222683, "base": "EUR", "date": "2023-07-24", "rates": { "USD": 1.107463, "EUR": 1 } }';

    /**
     * @covers ApiLayerManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testCurrencyConversionWithValidExchangeRate(): void
    {
        $amount = 100;
        $from = Currency::USD;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn(self::RESPONSE_CONTENT_BODY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ApiLayerManager($httpClient);
        $convertedAmount = $apiLayerManager->convert($amount, $from, $to);

        $this->assertEqualsWithDelta(90.29, $convertedAmount, 0.01);
    }

    /**
     * @covers ApiLayerManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testCurrencyConversionWithSameCurrencyValidExchangeRate(): void
    {
        $amount = 100;
        $from = Currency::EUR;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn(self::RESPONSE_CONTENT_BODY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ApiLayerManager($httpClient);
        $convertedAmount = $apiLayerManager->convert($amount, $from, $to);

        $this->assertEqualsWithDelta(100, $convertedAmount, 0.01);
    }

    /**
     * @covers ApiLayerManager::convert
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testException(): void
    {
        $this->expectException(InvalidApiResponseException::class);
        $amount = 100;
        $from = Currency::EUR;
        $to = Currency::EUR;

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn('{}');

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $apiLayerManager = new ApiLayerManager($httpClient);
        $apiLayerManager->convert($amount, $from, $to);
    }
}
