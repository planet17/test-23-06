<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\CountryResolvers;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Infrastructure\CountryResolvers\BinList;
use JsonException;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class BinListTest extends TestCase
{
    private const RESPONSE_CONTENT_BODY = '{ "number": { "length": 16, "luhn": true }, "scheme": "visa", "type": "debit", "brand": "Visa/Dankort", "prepaid": false, "country": { "numeric": "208", "alpha2": "DK", "name": "Denmark", "emoji": "🇩🇰", "currency": "DKK", "latitude": 56, "longitude": 10 }, "bank": { "name": "Jyske Bank", "url": "www.jyskebank.dk", "phone": "+4589893300", "city": "Hjørring" } }';

    /**
     * @covers BinList::resolve
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testResolveValidResponse(): void
    {
        $transaction = new Transaction('45717360', 100, Currency::EUR);

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn(self::RESPONSE_CONTENT_BODY);

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $countryApiManager = new BinList($httpClient);
        $country = $countryApiManager->resolve($transaction);

        $this->assertEquals('DK', $country->getCode());
    }

    /**
     * @covers BinList::resolve
     *
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testException(): void
    {
        $this->expectException(RuntimeException::class);
        $transaction = new Transaction('45717360', 100, Currency::EUR);

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->method('getContent')->willReturn('{}');

        $httpClient = $this->createMock(HttpClientInterface::class);
        $httpClient->method('request')->willReturn($mockResponse);

        $countryApiManager = new BinList($httpClient);
        $country = $countryApiManager->resolve($transaction);

        $this->assertEquals('DK', $country->getCode());
    }
}
