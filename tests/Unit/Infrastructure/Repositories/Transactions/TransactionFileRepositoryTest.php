<?php

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Repositories\Transactions;

use App\Domain\Dto\Transaction;
use App\Domain\Exception\InvalidTransactionRowException;
use App\Infrastructure\Repositories\Transactions\TransactionFileRepository;
use InvalidArgumentException;
use JsonException;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

class TransactionFileRepositoryTest extends TestCase
{
    private vfsStreamDirectory $fileSystem;

    protected function setUp(): void
    {
        $directory = [
            'valid.txt' =>
                '{"bin":"45717360","amount":"100.00","currency":"EUR"}' . PHP_EOL
                . '{"bin":"516793","amount":"50.00","currency":"USD"}' . PHP_EOL
            ,
            'not-found-amount.txt' =>
                '{"bin": "654321", "currency": "EUR"}' . PHP_EOL,
            'not-found-bin.txt' =>
                '{"amount": 50.0, "currency": "EUR"}' . PHP_EOL,
            'not-found-currency.txt' =>
                '{"amount": 50.0, "bin": "654321"}' . PHP_EOL,
            'invalid-amount.txt' =>
                '{"amount": "a", "bin": "654321", "currency": "EUR"}' . PHP_EOL,
            'invalid-bin.txt' =>
                '{"amount": 50.0, "bin": "a", "currency": "EUR"}' . PHP_EOL,
            'invalid-currency.txt' =>
                '{"amount": 50.0, "bin": "654321", "currency": "INVALID"}' . PHP_EOL,
        ];

        $this->fileSystem = vfsStream::setup('directory', 444, $directory);
    }

    /**
     * @covers TransactionFileRepository::all
     */
    public function testAll(): void
    {
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/valid.txt');
        $transactions = $repository->all();
        $this->assertIsArray($transactions);
        $this->assertContainsOnlyInstancesOf(Transaction::class, $transactions);
        $this->assertCount(2, $transactions);
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     * @throws JsonException
     */
    public function testNotFoundAmount(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/not-found-amount.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     */
    public function testNotFoundBin(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/not-found-bin.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     */
    public function testNotFoundCurrency(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/not-found-currency.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     */
    public function testInvalidAmount(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/invalid-amount.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     */
    public function testInvalidBin(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/invalid-bin.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::validateRowData
     */
    public function testInvalidCurrencyCode(): void
    {
        $this->expectException(InvalidTransactionRowException::class);
        $repository = new TransactionFileRepository($this->fileSystem->url() . '/invalid-currency.txt');
        $repository->all();
    }

    /**
     * @covers TransactionFileRepository::preValidate
     */
    public function testPreValidate(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new TransactionFileRepository($this->fileSystem->url() . '/file-not-found.txt');
    }
}
