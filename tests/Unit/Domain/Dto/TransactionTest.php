<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Dto;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    /**
     * @covers Transaction::getBin
     */
    public function testGetBin(): void
    {
        $bin = '123456';
        $amount = 100.0;
        $currency = Currency::EUR;
        $transaction = new Transaction($bin, $amount, $currency);
        $this->assertEquals($bin, $transaction->getBin());
    }

    /**
     * @covers Transaction::getAmount
     */
    public function testGetAmount(): void
    {
        $bin = '123456';
        $amount = 100.0;
        $currency = Currency::EUR;
        $transaction = new Transaction($bin, $amount, $currency);
        $this->assertEquals($amount, $transaction->getAmount());
    }

    /**
     * @covers Transaction::getCurrency
     */
    public function testGetCurrency(): void
    {
        $bin = '123456';
        $amount = 100.0;
        $currency = Currency::EUR;
        $transaction = new Transaction($bin, $amount, $currency);
        $this->assertEquals($currency, $transaction->getCurrency());
    }
}
