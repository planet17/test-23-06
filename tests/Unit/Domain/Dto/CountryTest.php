<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Dto;

use App\Domain\Dto\Country;
use PHPUnit\Framework\TestCase;

class CountryTest extends TestCase
{
    /**
     * @covers \App\Domain\Dto\Country
     */
    public function testGetCode(): void
    {
        $countryCode = 'UA';
        $country = new Country($countryCode);

        $this->assertEquals($countryCode, $country->getCode());
    }
}
