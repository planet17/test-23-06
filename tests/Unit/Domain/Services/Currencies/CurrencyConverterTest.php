<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Services\Currencies;

use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Domain\Services\Currencies\CurrencyConverter;
use App\Domain\Services\Currencies\CurrencyConverterManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

class CurrencyConverterTest extends TestCase
{
    /**
     * @covers CurrencyConverter::convert
     *
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testConvert(): void
    {
        $amount = 100;
        $from = Currency::USD;
        $to = Currency::EUR;
        $transaction = new Transaction('000000', $amount, $from);

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn(89.87);
        $service = new CurrencyConverter($manager);
        $converted = $service->convert($transaction, $to);
        $this->assertNotSame($transaction, $converted);
        $this->assertEqualsWithDelta(100, $transaction->getAmount(), 0.01);
        $this->assertEquals(Currency::USD, $transaction->getCurrency());
        $this->assertEqualsWithDelta(89.87, $converted->getAmount(), 0.01);
        $this->assertEquals(Currency::EUR, $converted->getCurrency());
    }

    /**
     * @covers CurrencyConverter::convert
     *
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testSame(): void
    {
        $amount = 100;
        $from = Currency::USD;
        $to = Currency::USD;
        $transaction = new Transaction('000000', $amount, $from);

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn(89.87);
        $service = new CurrencyConverter($manager);
        $converted = $service->convert($transaction, $to);
        $this->assertSame($transaction, $converted);
        $this->assertEqualsWithDelta(100, $transaction->getAmount(), 0.01);
        $this->assertEquals(Currency::USD, $transaction->getCurrency());
        $this->assertEqualsWithDelta(100, $converted->getAmount(), 0.01);
        $this->assertEquals(Currency::USD, $converted->getCurrency());
    }
}
