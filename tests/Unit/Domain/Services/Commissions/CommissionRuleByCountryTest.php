<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Services\Commissions;

use App\Domain\Dto\Country;
use App\Domain\Services\Commissions\CommissionRuleByCountry;
use PHPUnit\Framework\TestCase;

class CommissionRuleByCountryTest extends TestCase
{
    private const COMMISSION_EU = 0.01;
    private const COMMISSION_NON_EU = 0.02;

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testEu(): void
    {
        $country = new Country('LT');
        $service = new CommissionRuleByCountry();
        $commission = $service->match($country);
        $this->assertEquals(self::COMMISSION_EU, $commission);
    }

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testNonEu(): void
    {
        $country = new Country('JP');
        $service = new CommissionRuleByCountry();
        $commission = $service->match($country);
        $this->assertEquals(self::COMMISSION_NON_EU, $commission);
    }
}
