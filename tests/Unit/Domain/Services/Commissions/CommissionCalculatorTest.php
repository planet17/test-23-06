<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Services\Commissions;

use App\Domain\Dto\Country;
use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Domain\Services\Commissions\CommissionCalculator;
use App\Domain\Services\Commissions\CommissionConfiguration;
use App\Domain\Services\Commissions\CommissionRuleByCountry;
use App\Domain\Services\Commissions\CountryResolverInterface;
use App\Domain\Services\Currencies\CurrencyConverter;
use App\Domain\Services\Currencies\CurrencyConverterManagerInterface;
use PHPUnit\Framework\TestCase;

class CommissionCalculatorTest extends TestCase
{
    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testEuSame(): void
    {
        $transaction = new Transaction('000000', 100, Currency::EUR);
        $countryCode = 'LT';
        $converted = 85.87;

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $configuration = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn($converted);
        $converter = new CurrencyConverter($manager);

        $service = new CommissionCalculator($configuration, $converter, Currency::EUR,);

        $this->assertEqualsWithDelta(1.0, $service->calculate($transaction), 0.01);
    }

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testNonEuSame(): void
    {
        $transaction = new Transaction('000000', 100, Currency::EUR);
        $countryCode = 'JP';
        $converted = 89.87;

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $configuration = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn($converted);
        $converter = new CurrencyConverter($manager);

        $service = new CommissionCalculator($configuration, $converter, Currency::EUR,);

        $this->assertEqualsWithDelta(2.0, $service->calculate($transaction), 0.01);
    }

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testEu(): void
    {
        $transaction = new Transaction('000000', 100, Currency::USD);
        $countryCode = 'LT';
        $converted = 89.87;

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $configuration = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn($converted);
        $converter = new CurrencyConverter($manager);

        $service = new CommissionCalculator($configuration, $converter, Currency::EUR,);

        $this->assertEqualsWithDelta($converted / 100, $service->calculate($transaction), 0.01);
    }

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testNonEu(): void
    {
        $transaction = new Transaction('000000', 100, Currency::USD);
        $countryCode = 'JP';
        $converted = 89.87;

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $configuration = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $manager = $this->createMock(CurrencyConverterManagerInterface::class);
        $manager->method('convert')->willReturn($converted);
        $converter = new CurrencyConverter($manager);

        $service = new CommissionCalculator($configuration, $converter, Currency::EUR,);

        $this->assertEqualsWithDelta($converted / 100 * 2, $service->calculate($transaction), 0.01);
    }
}
