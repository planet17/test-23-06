<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Services\Commissions;

use App\Domain\Dto\Country;
use App\Domain\Dto\Transaction;
use App\Domain\Enums\Currency;
use App\Domain\Services\Commissions\CommissionConfiguration;
use App\Domain\Services\Commissions\CommissionRuleByCountry;
use App\Domain\Services\Commissions\CountryResolverInterface;
use PHPUnit\Framework\TestCase;

class CommissionConfigurationTest extends TestCase
{
    private const COMMISSION_EU = 0.01;
    private const COMMISSION_NON_EU = 0.02;

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testEu(): void
    {
        $transaction = new Transaction('000000', 100, Currency::EUR);
        $countryCode = 'LT';

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $service = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $this->assertEquals(self::COMMISSION_EU, $service->get($transaction));
    }

    /**
     * @covers \App\Domain\Services\Commissions\CommissionRuleByCountry::match
     */
    public function testNonEu(): void
    {
        $transaction = new Transaction('000000', 100, Currency::EUR);
        $countryCode = 'JP';

        $mockResponse = $this->createMock(CountryResolverInterface::class);
        $mockResponse->method('resolve')->willReturn(new Country($countryCode));
        $service = new CommissionConfiguration($mockResponse, new CommissionRuleByCountry());

        $this->assertEquals(self::COMMISSION_NON_EU, $service->get($transaction));
    }
}
